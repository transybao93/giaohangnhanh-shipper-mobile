import React, { 
    Component,
    Fragment,
    StrictMode,
 } from 'react';
import { 
    View,
    Button,
} from 'react-native';
import { 
    TextInput,
} from '@shoutem/ui';

//- create context
import { InputContext } from '../../context/LoginContext'
import ButtonComponent from './ButtonComponent'

class InputComponent extends Component {

    state = {
        email: '',
        password: ''
    }

    constructor(props)
    {
        super(props)
    }

    handlePress = () => {
        alert('email: ' + this.state.email + 'password: ' + this.state.password)
    }

    render() {
        return (
            <Fragment>
                <TextInput
                    placeholder={'Email'}
                    onChangeText={(email)=>this.setState({email})}
                    style={{
                        marginBottom: 10, 
                        height: 63, 
                        fontSize: 20
                    }}
                />
                <TextInput
                    placeholder={'Mật khẩu'}
                    onChangeText={(password)=>this.setState({password})}
                    secureTextEntry
                    style={{
                        marginBottom: 10, 
                        height: 63, 
                        fontSize: 20
                    }}
                />

                <ButtonComponent email={this.state.email} password={this.state.password}/>


            </Fragment>
        );
    }
}

export default InputComponent;
