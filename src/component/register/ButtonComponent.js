import React, { 
  Component,
  Fragment
} from 'react';
import { 
  View,
  Animated,
  TouchableHighlight,
  StyleSheet,

} from 'react-native';
import { 
  Button,
  Text,
  Spinner,
  Icon,
} from '@shoutem/ui';
import { InputContext } from '../../context/LoginContext'

class ButtonComponent extends Component {

  constructor(props)
  {

    super(props)
    this.state = {
      pressStatus: false
    }

  }

  _onHideUnderlay(){
    this.setState({ pressStatus: false });
  }
  _onShowUnderlay(){
    this.setState({ pressStatus: true });
  }

  handlePress = () => {

    alert('email: ' + this.props.email + ' password: ' + this.props.password)
    
  }
  render() {
    return (
      <Fragment>

        
          <Button 
            styleName="dark"
            onPress={this.handlePress}
          >
            <Icon name="checkbox-on" />
            <Text
              style={{fontSize: 20}}
            >
              Đăng ký
            </Text>
          </Button>

      </Fragment>
    );
  }
}

export default ButtonComponent;

const styles = StyleSheet.create({
  button: {
    borderColor: '#000066',
    borderWidth: 1,
    borderRadius: 10,
  },
  buttonPress: {
    borderColor: '#000066',
    // backgroundColor: '#000066',
    backgroundColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
  },
})
