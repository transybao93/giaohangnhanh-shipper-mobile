import React, { Component } from 'react';
import { 
  View, 
  Text,  
  StyleSheet,

} from 'react-native';
import { 
  ImageBackground,
} from '@shoutem/ui';
import {Actions} from 'react-native-router-flux'
import InputComponent from '../component/register/InputComponent'

class Register extends Component {
  render() {
    return (
      <View style={StyleSheet.flatten([styles.container])}>
        <ImageBackground
          styleName="large-portrait"
          style={{flex: 1, height: '100%'}}
          source={{ uri: 'https://images.unsplash.com/photo-1422393462206-207b0fbd8d6b?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=aa135db67893b4a1178843d1647a16ad&auto=format&fit=crop&w=1350&q=80' }}
        >

          <View style={styles.logo}>
              {/* Logo area */}
            </View>
            <View style={styles.main}>
              
              <InputComponent />

            </View>
            <View style={styles.button}></View>
          
        </ImageBackground>
      </View>
    );
  }
}

export default Register;

const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },

  main: {
    flex: 1,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
  },

  logo: {
    flex: 1.5,
    width: '100%',
    padding: 15
  },

  button: {
    flex: 1.25,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
  },


})
