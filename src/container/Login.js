import React, { Component } from 'react';
import { 
  View, 
  Text,
  StyleSheet,
  TouchableOpacity,

} from 'react-native';
import { 
  TextInput,
  Divider,
  Button,
  // Text,
  ImageBackground,
} from '@shoutem/ui';
import {Actions} from 'react-native-router-flux'
import InputComponent from '../component/login/InputComponent'
import ButtonComponent from '../component/login/ButtonComponent'
import { InputContext } from '../context/LoginContext'

class Login extends Component {
  render() {
    return (
      
        <View style={StyleSheet.flatten([styles.container])}>
          <ImageBackground
            styleName="large-portrait"
            style={{flex: 1, height: '100%'}}
            source={{ uri: 'https://images.unsplash.com/photo-1422393462206-207b0fbd8d6b?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=aa135db67893b4a1178843d1647a16ad&auto=format&fit=crop&w=1350&q=80' }}
          >
            <View style={styles.logo}>
              {/* Logo area */}
            </View>
            <View style={styles.main}>
              
              <InputComponent />

              <TouchableOpacity
                  onPress={() => {
                    Actions.register()
                  }}
                >
                <Text 
                  style={styles.forgotPasswordText}
                >
                  Chưa có tài khoản? 
                  
                    <Text
                      style={styles.anchorLink}
                    > 
                    Tạo tài khoản!
                    </Text>
                  
                </Text>
              </TouchableOpacity>


              <Text 
                style={styles.forgotPasswordText}
              >
                Quên mật khẩu? 
                <Text
                  style={styles.anchorLink}
                > Click vào đây</Text>
              </Text>

            </View>
            <View style={styles.button}></View>
          </ImageBackground>
        </View>
      
    );
  }
}

export default Login;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    // backgroundColor: 'white',
    alignItems: 'center',
  },

  main: {
    flex: 1,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
  },

  logo: {
    flex: 1.5,
    width: '100%',
    padding: 15
  },

  button: {
    flex: 1.25,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
  },

  textInput: {
    marginBottom: 10, 
    height: 63, 
    fontSize: 20
  },

  btnLogin: {
    marginBottom: 10, 
    height: 63
  },

  forgotPasswordText:{
    color: 'white',
    fontSize: 22,
    fontStyle: 'italic',
  },

  anchorLink: {
    color: 'yellow'
  },

})
