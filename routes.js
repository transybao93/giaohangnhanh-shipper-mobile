import React, { Component } from 'react';
import { 
    Router,
    Stack,
    Scene
  } from 'react-native-router-flux'
  import Login from './src/container/Login'
  import Register from './src/container/Register'

class Routers extends Component {
  render() {
    return (
        <Router>
        <Stack key="root" hideNavBar={true}>
          <Scene 
            key="login" 
            component={Login} 
            title="Login"
            initial
          />
          <Scene 
            key="register" 
            component={Register} 
            title="Register"
          />
        </Stack>
      </Router>
    );
  }
}

export default Routers;
